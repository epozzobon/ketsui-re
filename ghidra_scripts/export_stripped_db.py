#Saves a packed database and strips away the copyrighted binary

#@author Enrico Pozzobon
#@category ketsui-re
#@keybinding 
#@menupath 
#@toolbar 


from ghidra.framework.store.db import PackedDatabase
from ghidra.program.database import ProgramContentHandler
from ghidra.program.database import ProgramDB
from db import DBHandle
import java.io
import sys
import os
import struct
import jarray


def savePacked(ofile):
    self = object()
    
    if os.path.isfile(ofile):
        os.remove(ofile)
    
    project = state.getProject()
    projectData = project.getProjectData()
    rootFolder = projectData.getRootFolder()
    domainFile = rootFolder.getFiles()[0]
    program = domainFile.getDomainObject(self, True, False, monitor)
    program.saveToPackedFile(java.io.File(ofile), monitor)


def stripPacked(ifile, ofile):
    pdb = PackedDatabase.getPackedDatabase(java.io.File(ifile), monitor)
    db = pdb.open(monitor)
    chainBufferIds = db.getTable('File Bytes').getRecord(0).getBinaryData(3)
    chainBufferId = struct.unpack(">HI", chainBufferIds)[1]
    buf = db.getBuffer(chainBufferId)

    tid = db.startTransaction()
    buf.fill(0, buf.length(), 0)
    db.endTransaction(tid, True)

    if os.path.isfile(ofile):
        os.remove(ofile)

    PackedDatabase.packDatabase(db, 'ket', pdb.getContentType(), java.io.File(ofile), monitor)
    db.close()


d = state.getProject().getProjectData().getRootFolder().getProjectLocator().getLocation()
ifile = os.path.join(d, 'exported/ketsui_v100.u38.gzf')
ofile = ifile + '.stripped'

print("Exporting packed database file to %s" % ifile)
savePacked(ifile)

print("Exporting stripped packed database file to %s" % ifile)
stripPacked(ifile, ofile)
