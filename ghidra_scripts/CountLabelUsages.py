#Counts how many time each label appears

#@author Enrico Pozzobon
#@category ketsui-re
#@keybinding 
#@menupath 
#@toolbar 


fm = currentProgram.getFunctionManager()
rm = currentProgram.getReferenceManager()
ls = currentProgram.getListing()
st = currentProgram.getSymbolTable()

ranges = [
    range(0x400, 0x920),
    range(0x921, 0xd20),
    range(0xd21, 0x1120),
]

for r in ranges:
    for dest in r:
        for r in getReferencesTo(toAddr(dest)):
            rm.delete(r)


refcounts = []
for a in rm.getReferenceDestinationIterator(currentProgram.getMinAddress(), True):
    codeUnit = ls.getCodeUnitContaining(a)
    if codeUnit is not None:
        n = rm.getReferenceCountTo(a)
        for l in st.getSymbols(a):
            refcounts.append((a, n, l))

refcounts.sort(key=lambda o:o[1])
for a, n, l in refcounts:
    if n < 10:
        continue
    print("%12s  %d  %s" % (a, n, l))
