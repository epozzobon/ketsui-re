#Finds functions in ketsui and assigns the correct custom storage

#@author Enrico Pozzobon
#@category ketsui-re
#@keybinding 
#@menupath 
#@toolbar 


from ghidra.program.model.listing import ParameterImpl
from ghidra.program.model.data import ShortDataType
from ghidra.program.model.data import PointerDataType
from ghidra.program.model.data import UnsignedShortDataType
from ghidra.program.model.data import ByteDataType
from ghidra.program.model.data import UnsignedCharDataType
from ghidra.program.model.data import UnsignedIntegerDataType
from ghidra.program.model.symbol import SourceType
from ghidra.program.model.data import BooleanDataType
from ghidra.program.model.data import VoidDataType
from ghidra.program.model.listing import VariableStorage

af = currentProgram.getAddressFactory()
fm = currentProgram.getFunctionManager()
mem = currentProgram.getMemory()
dtm = currentProgram.getDataTypeManager()
st = currentProgram.getSymbolTable()

to_uint = lambda num: num & ((1<<32)-1)


reg_cf = currentProgram.getRegister("CF")

reg_d4b = currentProgram.getRegister("D4b")
reg_d5b = currentProgram.getRegister("D5b")

reg_d1w = currentProgram.getRegister("D1w")
reg_d6w = currentProgram.getRegister("D6w")
reg_d7w = currentProgram.getRegister("D7w")

reg_d4 = currentProgram.getRegister("D4")
reg_d5 = currentProgram.getRegister("D5")
reg_d6 = currentProgram.getRegister("D6")

reg_a0 = currentProgram.getRegister("A0")
reg_a4 = currentProgram.getRegister("A4")
reg_a5 = currentProgram.getRegister("A5")
reg_a6 = currentProgram.getRegister("A6")


dt_bool = BooleanDataType()
dt_enemy_ptr = dtm.findDataType("memory.bin/enemy *")
dt_bullet_ptr = dtm.findDataType("memory.bin/enemy_bullet *")
dt_sprent_ptr = dtm.findDataType("memory.bin/bullet_spritebatch_entry *")
dt_enemy_part_ptr = dtm.findDataType("memory.bin/enemy_part *")
dt_gamesched_entry_ptr = dtm.findDataType("memory.bin/gamesched_entry *")
dt_vector2 = dtm.findDataType("memory.bin/vector2")
dt_player_ptr = dtm.findDataType("memory.bin/player *")
dt_pbullet_ptr = dtm.findDataType("memory.bin/player_bullet *")


def make_enemy_function(a,n=2):
    return make_function(a, [ 
        (None          , VoidDataType()     , []      ),
        ("enemy"       , dt_enemy_ptr       , reg_a5  ),
        ("enemy_part"  , dt_enemy_part_ptr  , reg_a6  )
    ][n+1])
    #p3 = ("cf", dt_bool, reg_cf)


def make_schedulable_function(a):
    if a == 0:
        return None
    return make_function(a, [
        (None               , VoidDataType()             , []       ),
        ("gamesched_entry"  , dt_gamesched_entry_ptr     , reg_a5   ),
        ("param_2"          , UnsignedIntegerDataType()  , reg_d5   ),
        ("param_3"          , UnsignedIntegerDataType()  , reg_d6   ),
        ("param_4"          , UnsignedShortDataType()    , reg_d7w  )
    ])


def make_enemy_script_function(a):
    if a == 0:
        return None
    return make_function(a, [
        (None           , PointerDataType()   , [reg_a0]   ),
        ("script"       , PointerDataType()   , reg_a0     ),
        ("enemy"        , dt_enemy_ptr        , reg_a5     ),
        ("enemy_part"   , dt_enemy_part_ptr   , reg_a6     )
    ])


def make_into_bullet_updater(a):
    return make_function(a, [
        (None           , VoidDataType()   , []   ),
        ("sprite_entry" , dt_sprent_ptr       , reg_a4     ),
        ("bullet"       , dt_bullet_ptr       , reg_a6     ),
        ("camera_speed" , ShortDataType()     , reg_d6w    )
    ])


def make_into_bullet_constructor(a):
    return make_function(a, [
        (None       , VoidDataType()   , []   ),
        ("bullet"   , dt_bullet_ptr     , reg_a0  ),
        ("position" , dt_vector2        , reg_d4  ),
        ("angle"    , ByteDataType()    , reg_d5b )
    ])


def make_into_player_bullet_updater(a):
    return make_function(a, [
        (None       , VoidDataType()   , []   ),
        ("player"   , dt_player_ptr           , reg_a4  ),
        ("bullet"   , dt_pbullet_ptr          , reg_a6  ),
        ("flags"    , UnsignedShortDataType() , reg_d1w )
    ])


def make_function(a, signature):
    a = toAddr(a)
    f = fm.getFunctionAt(a)
    if f is None:
        print("Making 0x%s into a function" % a)
        f = createFunction(a, None)

    f.setCustomVariableStorage(True)

    while f.getParameterCount():
        f.removeParameter(0)

    result = signature[0]
    assert result[0] is None
    if result[2]:
        ret_stor = result[2]
        if not hasattr(ret_stor, "__len__"):
            ret_stor = [ ret_stor ];
        f.setReturn(result[1],
            VariableStorage(f.getProgram(), ret_stor),
            SourceType.USER_DEFINED)
    else:
        f.setReturnType(result[1], SourceType.USER_DEFINED)

    args = signature[1:]
    for a in args:
        p = ParameterImpl(a[0], a[1], a[2], f.getProgram())
        f.addParameter(p, SourceType.USER_DEFINED)

    return f

for i in range(16):
    fun = mem.getInt(toAddr(0xbb898+i*4))
    f = make_into_player_bullet_updater(fun)
   
"""
for i in range(74):
    bullet_def = to_uint(mem.getInt(toAddr(0xf8250+i*4)))
    bullet_ctor = to_uint(mem.getInt(toAddr(bullet_def + 18)))
    if bullet_ctor == 0:
        continue
    print(toAddr(bullet_ctor))

    f = make_into_bullet_constructor(bullet_ctor)

    if (f.getName().startswith("FUN_")):
        f.setName("bullet_%d_constructor" % i, SourceType.USER_DEFINED)
"""


"""
for i in range(74):
    bullet_def = to_uint(mem.getInt(toAddr(0xf8250+i*4)))
    bullet_fun = to_uint(mem.getInt(toAddr(bullet_def + 22)))
    print(toAddr(bullet_fun))

    f = make_into_bullet_updater(bullet_fun)
"""


"""
manual_toAddrs = [
    0xf85de, 0xf86d0, 0xf8956, 0xf8ea2, 0xfa0e8, 0xb111a, 0xb0a74, 0xb0a74,
    0xb0afe, 0xb0b88, 0xb0c12, 0xb0c9c, 0xb0d26, 0xb0db0, 0xb0e3a, 0xb0ec4,
    0xb0f4e, 0xfcd4a, 0xf7344, 0xf7344, 0xf747e, 0xf749c, 0xf74ba, 0xf74d8,
    0xf74f6, 0xf7514, 0xf7532, 0xf7550, 0xf756e, 0xf758c, 0xf7362, 0xf7380,
    0xf739e, 0xf73bc, 0xf73da, 0xf73f8, 0xf7416, 0xf7434, 0xf7452, 0xf83fc,
    0xf84ec, 0xf87a8, 0xf8b3a, 0xf8c2c, 0xf9362, 0xf94c8, 0xf95b2, 0xfb0c2,
    0xf96f8, 0xf985c, 0xf9954, 0xf9a58, 0xf9b1e, 0xf9bf8, 0xf9d14, 0xf9f54,
    0xfa286, 0xfa41a, 0xfa732, 0xfa83e, 0xfa998, 0xfaa4c, 0xfaa96, 0xfab96,
    0xfac8e, 0xfae2c, 0xfaf8a, 0xfb020, 0xfb1de, 0xfb24c, 0xfb2ba, 0xfb32c,
    0xfb3f8, 0xfb4ac, 0xfb5e8, 0xfb686, 0xfb728, 0xfb7a2, 0xfb87a, 0xfb9de,
    0xfbaca, 0xfbc30, 0xfbf20, 0xfbfd0, 0xfc0ac, 0xfc15e, 0xfc1fc, 0xfc2d8,
    0xfc384, 0xfc488, 0xfc5c6, 0xfc77a, 0xfc824, 0xfc8be, 0xfc9ac, 0xfcab4,
    0xfcb5a, 0xfcc4a, 0xfcd02, 0xfcd76, 0xfce36, 0xfcedc, 0xfcf78, 0xfcfd8,
    0xf8a48, 0xfa5ae, 0xf8d04, 0xf91ce
]

for a in manual_toAddrs:
    bullet_fun = to_uint(mem.getInt(toAddr(a + 2)))
    print(toAddr(a), toAddr(bullet_fun))
    f = make_into_bullet_updater(bullet_fun)

    if (f.getName().startswith("FUN_")):
        f.setName("bullet_%d_firstupdater" % i, SourceType.USER_DEFINED)
"""


"""
for i, a in enumerate(range(0xa9894, 0xa992c, 4)):
    fa = mem.getInt(toAddr(a))
    f = make_function(fa, [
        (None,     UnsignedShortDataType(), reg_d1w),
        ("number", UnsignedShortDataType(), reg_d1w)
    ])
    f.setName("mul_D1w_by_%d" % i, SourceType.USER_DEFINED)


for i, a in enumerate(range(0xa9b06, 0xa9b9e, 4)):
    fa = mem.getInt(toAddr(a))
    f = make_function(fa, [
        (None,     UnsignedShortDataType(), reg_d7w),
        ("number", UnsignedShortDataType(), reg_d7w)
    ])
    f.setName("mul_D7w_by_%d" % i, SourceType.USER_DEFINED)
"""

"""
boss_funcs = [
    ('midboss1', 0x12fc04, 0x130446, 0x12ecd2, 0x12ef9a, 0x12f1d2),
    ('boss1',    0x10dca0, 0x111588, 0x10cf1e, 0x10fd9e, 0x10e176),
    ('midboss2', 0x131612, 0x13255e, 0x131004, 0x132c16, 0x13199c),
    ('boss2',    0x113b76, 0x116560, 0x112d92, 0x11553a, 0x114538),
    ('midboss3', 0x134386, 0x137cec, 0x133376, 0x1368ba, 0x13457a),
    ('boss3',    0x11a73c, 0x11ce0c, 0x119a26, 0x11cc52, 0x11ad44),
    ('midboss4', 0x13a132, 0x13a48e, 0x138d0c, 0x13a304, 0x139142),
    ('boss4',    0x120086, 0x121e18, 0x11fa38, 0x122e9a, 0x120612),
    ('midboss5', 0x13bfda, 0x13d6ac, 0x13c7be, 0x13d43e, 0x13b188),
    ('boss5',    0x1267b8, 0x12b89a, 0x125a6c, 0x128552, 0x126e50)
]

first_argument_datatypes = [
    dtm.findDataType("memory.bin/struct_742 *"),
    dtm.findDataType("memory.bin/enemy_shooting_state *"),
    dtm.findDataType("memory.bin/struct_241 *"),
    dtm.findDataType("memory.bin/struct_028 *"),
    dtm.findDataType("memory.bin/struct_199 *"),
]

for name, fns1, fns2, fns3, fns4, fns5 in boss_funcs:
    print(name)
    st.createLabel(toAddr(fns1), name+'_movement_functions', SourceType.USER_DEFINED)
    st.createLabel(toAddr(fns2), name+'_shooting_functions', SourceType.USER_DEFINED)
    st.createLabel(toAddr(fns3), name+'_render_functions', SourceType.USER_DEFINED)
    st.createLabel(toAddr(fns4), name+'_functions_4', SourceType.USER_DEFINED)
    st.createLabel(toAddr(fns5), name+'_functions_5', SourceType.USER_DEFINED)
    for j, fns in enumerate([fns1, fns2, fns3, fns4, fns5]):
        dt_param_1 = first_argument_datatypes[j]

        i = 0
        while 1:
            fa = mem.getInt(toAddr(fns + i))
            print(hex(fa))
            if ((fa & 0xfff00000) != 0x00100000):
                break

            f = make_function(fa, [
                (None         , VoidDataType()      , []     ),
                ("param_1"    , dt_param_1          , reg_a4 ),
                ("enemy"      , dt_enemy_ptr        , reg_a5 ),
                ("enemy_part" , dt_enemy_part_ptr   , reg_a6 )
            ])
            fname = f.getName()
            if not fname.startswith(name):
                print("switching name of %s" % fname)
                fname = name + '_' + fname
                f.setName(fname, SourceType.ANALYSIS)


            i += 4
"""

"""
for a in range(0xcfd86, 0xd0186, 8):
#for a in range(0xf4730, 0xf4b30, 8):
    print("enemy 0x%x" % a)

    preallocator_fun = to_uint(mem.getInt(toAddr(a)))
    constructor_fun = 8 + preallocator_fun
    updater_fun = to_uint(mem.getInt(toAddr(a+4)))

    f1 = make_enemy_function(preallocator_fun, 1)
    f2 = make_enemy_function(constructor_fun, 2)
    f3 = make_enemy_function(updater_fun, 2)
"""


"""
for a in range(0xac8ac, 0xac994, 8):
    print("schedulable 0x%x" % a)

    schedulable_fun = to_uint(mem.getInt(toAddr(a)))
    f = make_schedulable_function(schedulable_fun)
"""


"""
for a in range(0xcfcb0, 0xcfce0, 4):
    print("scriptfn 0x%x" % a)

    scriptfn = to_uint(mem.getInt(toAddr(a)))
    f = make_enemy_script_function(scriptfn)
"""
