# ketsui-re

Reverse engineering of the videogame *Ketsui: Kizuna Jigoku Tachi* using Ghidra.

## Before starting

I am not associated with Cave in any way and I don't claim any copyright over Ketsui.

I'm trying to keep all material copyrighted by Cave outside of this repository. As such, I stripped the ROM file from the Ghidra packed database file `exported/ketsui_v100.u38.gzf.stripped`.

If you have legally obtained a copy of the `ket` romset for mame, you can extract it in the `ket` directory and run `ghidra_scripts/unstrip_db.py` to produce a usable packed database file.

To do this:

- Extract the ket mame romset in the `ket` directory of this repository.
- Open Ghidra, and create a new project in the root directory of this repository.
- Select Tools > Run Tool > CodeBrowser.
- In the CodeBrowser, select Window > Script Manager.
- Add the `ghidra_scripts` directory included in this repository to your Ghidra Script Directories.
- Run the `ketsui-re/unstrip-db.py` script from within the Script Manager. This should produce the file `exported/ketsui_v100.u38.gzf`.
- Close the CodeBrowser.
- In the project window of Ghidra, use "File > Import File..." to open `exported/ketsui_v100.u38.gzf`.

## About Ketsui

*Ketsui: Kizuna Jigoku Tachi* is an Shoot'em'up game developed by Cave.

Released in 2003, Ketsui ran on the PolyGame Master (PGM) arcade platform, which has less computational power than the average toaster. To put this into perspective, Ketsui runs on a 20MHz processor on hardware which is incapable of doing even sprite rotation, while home computers of the same era were equipped with 2GHz CPUs and ran Halo.

Ketsui's board includes a motorola 68000 processor which acts as the main CPU, a Z80 processor responsible for sound, and an ARM "pgmprot" core completely dedicated to the protection of the game from bootleggers. The encryption scheme used here is a simple XOR pad as shown on the mame file [pgmcrypt.cpp](https://github.com/mamedev/mame/blob/master/src/mame/machine/pgmcrypt.cpp). At various points in the program, Ketsui exchanges data to the pgmprot core via the memory mapped registers at [0x400000:0x400007] to make sure that the program will not work if the protection core is not on the board.

Additional information about the PGM can be found on the [PGM tech info](http://www.gc8tech.com/pgm-tech-info/) page.

## Some interesting discoveries

### Hitboxes

All bullets have the same hitbox size. This might sound odd, since the sprites are clearly different for each bullet, but the code that checks for the collisions does not distinguish between the different bullet types.

The collision algorithm (0xb109c) is simply checking if the centers of the player and a bullet come within a range of 3 pixels on the Y axis of 2 pixels on the X axis. Notice that the pixels in Ketsui are not square, but instead have a 2x3 aspect ratio:

- Screen aspect ratio: 4:3

- Screen resolution: 448x224 (2:1)
- Pixel aspect ratio: 3:2

So the it turns out that the player hitbox can be seen as a 6x4 pixels square, and the player is hit only when the center of an enemy bullet enters that square. The player hitbox is set by the function at 0xb5d70 and is composed of 4 words in [0x80feb2:0x80feba]. These correspond to the extension of the player hitbox in the [top, bottom, right, left] directions respectively in subpixels (a subpixel corresponds to 1/64th of a pixel).

The hitbox shrinks to a 6x3.5 pixels big rectangle when entering the second game loop.

### Enemy scripting

Cave implemented a small "scripting language" to script the actions of most enemies in the game. Each enemy executes one or more lines of its script on every frame when the enemy is alive. For example, the scripts for the enemies in the first stage of the game are stored at locations  [0x07d512:0x07df47].

The length of each "line" of these scripts is always an even number of bytes. The first byte in every line indicates the instruction (such as set speed, set angle, wait N frames, set some variable, loop back N lines...), while every other byte in the line contains parameters for the instruction.

There is also a list of all the enemies that will spawn at any given moment in time (for the first stage this list is at [0x07c7c0:0x07d413]). It would be easy to build a randomizer tool for Ketsui which just changes the order in which the enemies and the bosses are spawned by modifying this list.

## Hints

Reverse engineering Ketsui with Ghidra presents several challenges, so here are some useful things to keep in mind.

### Calling conventions

The programming of Ketsui does not always follow the 68k calling convention (which would be to put all arguments on the stack and the return value in D0). Since Ghidra expects the standard calling convention to be the used, the decompiler will almost always produce unusable code out of the box. To fix this, you may enable the "Use Custom Storage" flag on the function you are analyzing and manually set the arguments storage.

For example, let's look at the function `polar2cartesian_lookup` at [0xad274:0xad28f], which uses a lookup table to convert polar coordinates into cartesian coordinates:

- The magnitude argument is passed by register in D0w
- The angle argument is passed by register in D1b
- The Y coordinate is returned by register in D2w
- The X coordinate is returned by register in D3w

So you can see that all arguments and returned variables are stored in registers, and a function can return more than one value.

On top of that, it is often the case that the Carry Flag (CF) in the Status Register (SR) is used by functions to return whether the function was successful or not. This can even happen implicitly, without any code setting CF, just by the last instruction of the function having a side effect on the CF.

### Jump tables

I don't know if this was their compiler transforming switch statements into jump tables or if they did this by design, but if you take a look at the disassembly you will often find arrays of function pointers.

One interesting example here is [0x0a9894:0xa9993], containing function pointers to code in the rage [0x0a9994:0x0a9b05], which contains functions for multiplying the number in register D1w by any number between 1 and 37. There is another identical table at [0xa9b06:0xa9c05] pointing in the range [0xa9c06:0xa9d69], doing exactly the same multiplications on D7w instead.

These jump tables are a big pain for the decompiler, since in Ghidra it is not possible to specify custom argument storage for function pointers (see issue [428](https://github.com/NationalSecurityAgency/ghidra/issues/428)). As such, any function that jumps to a function pointer might look broken in the decompiler and should be reverse engineered from the disassembly instead.