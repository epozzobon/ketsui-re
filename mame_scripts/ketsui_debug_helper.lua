

-- dofile('ketsui_debug_helper.lua')
--
-- bpset 0xcf9ca

machine = manager:machine()
dbg = machine:debugger()
cpu = machine.devices[":maincpu"]
mem = cpu.spaces["program"]
screen = machine.screens[":screen"]

function exec_periodic()
    funbox.periodic()
end

function exec_frame_done()
    funbox.frame_done()
end

if not funbox then
    funbox = {}
    print('defined funbox')
    emu.register_periodic(exec_periodic)
    emu.register_frame_done(exec_frame_done)
end

known_enemies = {
    [0x00]={name='null'},
    [0x06]={name='null'},
    [0x07]={name='null'},
    [0x08]={name='null'},
    [0x09]={name='null'},
    [0x0a]={name='null'},
    [0x0b]={name='null'},
    [0x0c]={name='null'},
    [0x0d]={name='null'},
    [0x0e]={name='yellow tank'},
    [0x10]={name='popcorn helicopter'},
    [0x11]={name='popcorn helicopter'},
    [0x12]={name='popcorn helicopter'},
    [0x13]={name='popcorn helicopter'},
    [0x14]={name='popcorn helicopter'},
    [0x15]={name='popcorn helicopter'},
    [0x16]={name='popcorn helicopter'},
    [0x17]={name='popcorn helicopter'},
    [0x18]={name='popcorn thincopter'},
    [0x1a]={name='popcorn plane'},
    [0x1c]={name='popcorn plane'},
    [0x1d]={name='popcorn plane'},
    [0x1f]={name='popcorn plane'},
    [0x20]={name='enemy spawner'},
    [0x21]={name='enemy spawner'},
    [0x22]={name='enemy spawner'},
    [0x23]={name='enemy spawner'},
    [0x24]={name='popcorn small tank'},
    [0x25]={name='tank'},
    [0x26]={name='helicopter twin rotor'},
    [0x27]={name='truck'},
    [0x28]={name='truck'},
    [0x29]={name='truck'},
    [0x2a]={name='red helicopter'},
    [0x2b]={name='red helicopter'},
    [0x2c]={name='large blue tank'},
    [0x2d]={name='large blue tank'},
    [0x2e]={name='large blue tank'},
    [0x2f]={name='large white tank'},
    [0x30]={name='popcorn helicopter'},
    [0x31]={name='popcorn helicopter'},
    [0x32]={name='popcorn helicopter'},
    [0x33]={name='popcorn helicopter'},
    [0x34]={name='popcorn helicopter'},
    [0x35]={name='popcorn helicopter'},
    [0x36]={name='popcorn helicopter'},
    [0x37]={name='popcorn helicopter'},
    [0x38]={name='popcorn thincopter'},
    [0x3a]={name='popcorn plane'},
    [0x40]={name='large white tank'},
    [0x42]={name='tiltrotor'},
    [0x43]={name='gray helicopter'},
    [0x44]={name='gray helicopter'},
    [0x45]={name='small twin turret'},
    [0x46]={name='gate'},
    [0x47]={name='gate'},
    [0x48]={name='ground turret'},
    [0x4a]={name='small silos'},
    [0x4b]={name='silos'},
    [0x4c]={name='popcorn hovercraft'},
    [0x4d]={name='popcorn hovercraft'},
    [0x4e]={name='submarine'},
    [0x50]={name='submarine'},
    [0x51]={name='submarine'},
    [0x52]={name='blue helicopter'},
    [0x53]={name='plane 4props'},
    [0x55]={name='tank artillery'},
    [0x56]={name='tank artillery'},
    [0x57]={name='popcorn tank'},
    [0x58]={name='popcorn tank'},
    [0x59]={name='nightmare'},
    [0x5a]={name='roomba'},
    [0x5e]={name='beige tank'},
    [0x5f]={name='beige tank h'},
    [0x60]={name='boss4 grenade'},
    [0x61]={name='boss4 option'},
    [0x62]={name='boss4 spawn animation d'},
    [0x63]={name='midboss5'},
    [0x64]={name='boss5'},
    [0x65]={name='boss5 option1'},
    [0x66]={name='boss5 option2'},
    [0x67]={name='boss5 doom option'},
    [0x70]={name='boss1'},
    [0x71]={name='boss1 option'},
    [0x72]={name='boss1 rocket'},
    [0x74]={name='midboss2'},
    [0x75]={name='midboss2 mine'},
    [0x76]={name='boss3'},
    [0x77]={name='midboss4'},
    [0x78]={name='midboss4 torpedo'},
    [0x79]={name='boss4'},
    [0x7a]={name='boss4 option side'},
    [0x7b]={name='boss4 carrier copter'},
    [0x7c]={name='boss4 spawn animation c'},
    [0x7d]={name='boss4 transformation animation'},
    [0x7e]={name='boss4 mine'},

    [0x80]={name='midboss1'},
    [0x82]={name='midboss1 option'},
    [0x84]={name='plane'},
    [0x85]={name='popcorn antiair'},
    [0x86]={name='turret roof'},
    [0x87]={name='turret roof'},
    [0x88]={name='turret'},
    [0x89]={name='turret'},
    [0x8a]={name='turret skyscraper'},
    [0x8b]={name='turret skyscraper'},
    [0x8d]={name='animated background'},
    [0x90]={name='boss2'},
    [0x91]={name='boss warning sign'},
    [0x92]={name='boss1 hangar'},
    [0x93]={name='boss2 mine'},
    [0x94]={name='boss2 grenade'},
    [0x95]={name='turret twin gun'},
    [0x96]={name='plane a'},
    [0x98]={name='red plane'},
    [0x99]={name='air conditioner'},
    [0x9a]={name='p1lookalike'},
    [0x9b]={name='p2lookalike'},
    [0x9d]={name='turret'},
    [0x9e]={name='enemy spawner'},
    [0xa0]={name='midboss3'},
    [0xa1]={name='boss5 doom spawn'},

    [256]={}
}


function print_enemy(enemy_addr)
    updater_id = mem:read_u8(enemy_addr + 12)
    updater = mem:read_u32(enemy_addr + 76)
    parts_count = 1+mem:read_u16(enemy_addr + 4)
    parts_addr = mem:read_u32(enemy_addr + 6)
    
    if (known_enemies[updater_id] ~= nil) then
        name = known_enemies[updater_id].name
        print(string.format("0x02%x: %s", updater_id, name))
        return true
    end

    print(string.format(
        'Enemy at address 0x%x, updater %d:%d@0x%x, %d parts at 0x%x',
        enemy_addr, (updater_id >> 7) & 1, updater_id & 0x7f, updater, parts_count, parts_addr
    ))

    for i = 0,parts_count-1 do
        part_addr = parts_addr + i * 42
        sprite = mem:read_u32(part_addr + 10)
        x = mem:read_i16(part_addr + 4)
        y = mem:read_i16(part_addr + 2)
        hp = mem:read_u16(part_addr + 24)
        print(string.format(
            ' - Part %d at (%d,%d), sprite 0x%x, hp:%d',
            i, x, y, sprite, hp
        ))
    end

    return false
end

funbox.periodic = function()
    if dbg.execution_state ~= 'stop' then return end
    pc = cpu.state['PC'].value
    if pc ~= 0xcf9ca then return end

    enemy_addr = cpu.state['A5'].value

    if (print_enemy(enemy_addr)) then
        dbg.execution_state = 'start'
    end

    print('----------------------------')
end

funbox.frame_done = function()
    -- make player invulnerable
    mem:write_u8(0x80fedc, 80)

    -- write some info to the hud
    num_batches = mem:read_u16(0x81a626)
    slowdown_frames = mem:read_u16(0x801118)
    num_frames = mem:read_u8(0x801138)
    enemy_bullets = mem:read_u16(0x81a61e)
    total_bullets = enemy_bullets + mem:read_u16(0x8113b4) + mem:read_u16(0x8113b6)

    screen:draw_box(1, 2+enemy_bullets/2, 9, 2+num_batches*10, 0x7fffffff)
    screen:draw_box(1, 2, 9, 2+enemy_bullets/2, 0x7f0000ff)
    screen:draw_box(6, 2+enemy_bullets/2, 9, 2+total_bullets/2, 0x7f007f00)

    if (num_frames == 0) then
        screen:draw_box(11, 2, 19, 8, 0xff00ffff) 
    elseif (num_frames > 1) then
        screen:draw_box(11, 2, 19, 8, 0xffff00ff) 
    end
    screen:draw_box(11, 10, 19, 10+slowdown_frames*2, 0x7fff0000)

end
